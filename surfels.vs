#version 330

in vec3 surfel;
in vec3 normal;

 vec3 eyeSurf;
out vec3 norm;
out vec3 tcoord;
out vec3 goraudColor;
out float specColor;

uniform mat4 mvp;
uniform mat4 mv;
uniform mat3 mn;
uniform vec3 lightPosition;

uniform float size;
uniform float shininess=1;

void main() {

    gl_Position = mvp*vec4(surfel,1);
    eyeSurf     = (mv*vec4(surfel,1)).xyz;
    norm        = normalize(mn*normal);
    tcoord      = surfel;

    vec3 N = norm;
    vec3 L = normalize(lightPosition-eyeSurf);

    vec3 lightColor = vec3(0.2, 0.5, 0.2);
    float diffuse = max(dot(L, N), 0.0);
    lightColor += vec3(diffuse, 0.0, 0.0);

    vec3 V = normalize(-eyeSurf);
    //vec3 H = normalize(L + V);
    //float specular = pow(max(dot(N, H), 0.0), shininess);
    vec3 R = reflect(-L, N);
    float specular = pow(max(dot(R, V), 0.0), shininess);

    specColor = specular;
    goraudColor = lightColor;
};
