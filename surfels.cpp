#include "pgr.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <fstream>
#include <random>
#ifndef SHADERS

#define SHADERS "../"
#endif //SHADERS

using namespace std;

// Just a stup input for maybe noise texture
GLuint seed_in[40000] = {
};

struct surfel_s {
    float surfel[3];
    float normal[3];
};

//struct surfel_s surfel_cloud[2*sizeof(struct surfel_s)*sizeof(seed_in)/sizeof(seed_in[0])];
struct surfel_s surfel_cloud[100]; //= { {{1,1,1},{1,1,1}},{{1,2,1},{1,1,2}},{{2,1,1},{2,2,1}} };

    // so far surface or (axx + byy + czz + dx + ey + fz + gh = 0)
struct sufrace_s {
    float a; float b; float c;
    float d; float e; float f;
    float g; float h;
};


////
// Control variables
////

int width, height;
int tsize = 2;
float rx = 0.0f, ry = 0.0f, pz = -70.0f;
float wheel = -10.0f;


enum Mode {
    ORTHOGONAL, PERSPECTIVE
} mode = PERSPECTIVE;

enum DrawMode {
    P_TRIANGELIZE, P_DEF

} draw_type = P_DEF;

bool depth = true;
bool ray_object_changed = true;

GLenum func = GL_LESS;

////////////////////////////////////////////////////////////////////////////////
// Shaders
////////////////////////////////////////////////////////////////////////////////

GLuint rc_VS, rc_GS, raycast_prog;
GLuint boxUniform, surfaceUniform, seedAttrib;

GLuint VS, GS, FS, GVS, draw_prog;
GLuint surfelAttrib, normalAttrib;
GLuint lightPositionUniform, mvUniform, mnUniform, mvpUniform, sizeUniform;

GLuint VBO;
GLuint TBO;

GLuint raycast_VAO;
GLuint draw_VAO;

const GLchar* feedbackVaryings[]= { "surfel", "normal" };

GLuint query;
GLuint primitives;

////////////////////////////////////////////////////////////////////////////////
// Event handlers
////////////////////////////////////////////////////////////////////////////////

void onInit()
{
    ofstream f;
    ofstream dbg;
    f.open("log.out");    // Shader
    dbg.open("dbg.out");
    int x = 0;

    try {
        rc_VS = compileShader(GL_VERTEX_SHADER, loadFile(SHADERS"raycast.vs").c_str());
        x = 1;
        rc_GS = compileShader(GL_GEOMETRY_SHADER, loadFile(SHADERS"raycast.gs").c_str());

        x = 2;
        VS = compileShader(GL_VERTEX_SHADER, loadFile(SHADERS"surfels.vs").c_str());
        x = 3;
        FS = compileShader(GL_FRAGMENT_SHADER, loadFile(SHADERS"surfels.fs").c_str());
        x = 4;
        GVS = compileShader(GL_VERTEX_SHADER, loadFile(SHADERS"surfels_xform.vs").c_str());
        x = 5;
        GS = compileShader(GL_GEOMETRY_SHADER, loadFile(SHADERS"surfels_xform.gs").c_str()); //use if xform points to triangles

    } catch(exception &e) {
        f << "Shader no: "<< x << endl << e.what() << endl;
    }

    try {
        raycast_prog = glCreateProgram();

        if (raycast_prog == 0) {
            f << "Exception for raycast_prog: Failed to create program" << endl;
        }

        glAttachShader(raycast_prog, rc_VS);
        glAttachShader(raycast_prog, rc_GS);

        glTransformFeedbackVaryings(raycast_prog, 2, feedbackVaryings, GL_INTERLEAVED_ATTRIBS);

        glLinkProgram(raycast_prog);

        int linkStatus = GL_TRUE;
        glGetProgramiv(raycast_prog, GL_LINK_STATUS, &linkStatus);
        if (linkStatus == GL_FALSE) {
            f << "Exception for raycast_prog: Linking failed for whatever reason." << endl;
            f << string(getProgramInfoLog(raycast_prog)).c_str() << endl;
        }

        if (draw_type == P_DEF) {
            draw_prog = linkShader(2, VS, FS);
        } else {
            draw_prog = linkShader(3, GVS, GS, FS);
        }
    } catch(exception &e) {
        f << "Exception for draw_prog: " << e.what() << endl;
    }

    int linkStatus = GL_TRUE;
    glGetProgramiv(draw_prog, GL_LINK_STATUS, &linkStatus);
    if (linkStatus == GL_FALSE) {
        f << "Exception for draw_prog: Linking failed for whatever reason." << endl;
    }

    dbg << glewGetErrorString(glGetError()) << endl;
    dbg << int(boxUniform = glGetUniformLocation(raycast_prog, "ray_box"));
    dbg << endl;
    dbg << int(surfaceUniform = glGetUniformLocation(raycast_prog, "surface"));
    dbg << endl;

    dbg << int(mvpUniform = glGetUniformLocation(draw_prog, "mvp"));
    dbg << endl;
    dbg << int(mvUniform = glGetUniformLocation(draw_prog, "mv"));
    dbg << endl;
    dbg << int(mnUniform = glGetUniformLocation(draw_prog, "mn"));
    dbg << endl;
    dbg << int(sizeUniform = glGetUniformLocation(draw_prog, "size"));
    dbg << endl;
    dbg << int(lightPositionUniform = glGetUniformLocation(draw_prog, "lightPosition"));
    dbg << endl;
    dbg << "Uniforms: "<< glewGetErrorString(glGetError()) << endl;

    dbg << int(seedAttrib = glGetAttribLocation(raycast_prog, "seed"));
    dbg << endl;
    dbg << int(surfelAttrib = glGetAttribLocation(draw_prog, "surfel"));
    dbg << endl;
    dbg << int(normalAttrib = glGetAttribLocation(draw_prog, "normal"));
    dbg << endl;
    dbg << "Attribs: "<< glewGetErrorString(glGetError()) << endl;

    glGenVertexArrays(1, &raycast_VAO);
    glBindVertexArray(raycast_VAO);

    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    mt19937 generator;
    uniform_int_distribution<unsigned> distribution(0U,0U-1);
    for (int x = 0; x < sizeof(seed_in)/sizeof(*seed_in); x++)
        seed_in[x] = distribution(generator);

    glBufferData(GL_ARRAY_BUFFER, sizeof(seed_in), seed_in, GL_STATIC_DRAW);
    glVertexAttribIPointer(seedAttrib, 1, GL_UNSIGNED_INT, 0, 0);

    // TBO buffer operations
    glGenVertexArrays(1, &draw_VAO);
    glBindVertexArray(draw_VAO);

    glGenBuffers(1, &TBO);
    glBindBuffer(GL_ARRAY_BUFFER, TBO);
    glBufferData(GL_ARRAY_BUFFER, 128*sizeof(struct surfel_s)*sizeof(seed_in)/sizeof(seed_in[0]),
                 NULL, GL_DYNAMIC_COPY);
    glVertexAttribPointer(surfelAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(struct surfel_s), (void*)offsetof(struct surfel_s, surfel));
    glVertexAttribPointer(normalAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(struct surfel_s), (void*)offsetof(struct surfel_s, normal));

    f.close();
    dbg << glewGetErrorString(glGetError()) << endl;
    dbg.close();

    ray_object_changed = true;
}

void onWindowRedraw()
{

    //ofstream f2;
    //f2.open("rendInfo.out");    // Shader

    if(ray_object_changed) {

        ofstream f;
        f.open("redraw-dbg.out");    // Shader

        GLfloat box=50;
        GLfloat surface=wheel*10;

        // Create query object to collect info
        glGenQueries(1, &query);

        glEnable(GL_RASTERIZER_DISCARD);
        glUseProgram(raycast_prog);

        glUniform1f(boxUniform, box);
        glUniform1f(surfaceUniform, surface);

        glBindVertexArray(raycast_VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glEnableVertexAttribArray(seedAttrib);

        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, TBO);
        glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);
            glBeginTransformFeedback(GL_POINTS);
               // glDrawArrays(GL_POINTS, 0, sizeof(seed_in)/sizeof(seed_in[0]));
                glDrawArrays(GL_POINTS, 0, 40000);
            glEndTransformFeedback();
        glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

        glDisableVertexAttribArray(seedAttrib);
        glDisable(GL_RASTERIZER_DISCARD);
        glFlush();

        glGetQueryObjectuiv(query, GL_QUERY_RESULT, &primitives);
//        glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, sizeof(surfel_cloud), surfel_cloud);

        f << "INFO: Points written: "<< primitives << endl; // Why zero ?

/*        for (unsigned i = 0; i < primitives; i++) {
            f << surfel_cloud[i].surfel[0] << ", ";
            f << surfel_cloud[i].surfel[1] << ", ";
            f << surfel_cloud[i].surfel[2] << "; ";
            f << surfel_cloud[i].normal[0] << ", ";
            f << surfel_cloud[i].normal[1] << ", ";
            f << surfel_cloud[i].normal[2] << endl;
        }
        */
        f << glewGetErrorString(glGetError()) << endl;
        ray_object_changed = false;

        f.close();
        //glBindBuffer(GL_ARRAY_BUFFER, TBO);
        //glBufferData(GL_ARRAY_BUFFER, sizeof(surfel_cloud), surfel_cloud, GL_STATIC_DRAW);
        glUseProgram(draw_prog);

        glUniform3f(lightPositionUniform, 20.0f, 20.0f, 0.0f);

        glBindVertexArray(draw_VAO);
        glBindBuffer(GL_ARRAY_BUFFER, TBO);
    }

//-------------------------------------------------------
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(depth) {
        glEnable(GL_DEPTH_TEST);
    } else {
        glDisable(GL_DEPTH_TEST);
    }

    glDepthFunc(func);

    // MVP
    glm::mat4 projection;

    float aspect = (float)width/(float)height;

    if(mode == ORTHOGONAL) {
        projection = glm::ortho(-pz*aspect, pz*aspect, -pz, pz, 1.0f, 1000.0f);
    } else {
        projection = glm::perspective(45.0f, (float)width/(float)height, 1.0f, 1000.0f);
    }

    glm::mat4 mv = glm::rotate(
            glm::rotate(
                glm::translate(
                    glm::mat4(1.0f),
                    glm::vec3(0, 0, pz)
                    ),
                ry, glm::vec3(1, 0, 0)
                ),
            rx, glm::vec3(0, 1, 0)
            );

    glm::mat4 mvp = projection*mv;

    glm::mat3 mn = glm::mat3(
            glm::rotate(
                    glm::rotate(
                            glm::mat4(1.0f),
                            ry, glm::vec3(1, 0, 0)
                    ),
                    rx, glm::vec3(0, 1, 0)
            )
    );

    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, glm::value_ptr(mvp));
    glUniformMatrix4fv(mvUniform, 1, GL_FALSE, glm::value_ptr(mv));
    glUniformMatrix3fv(mnUniform, 1, GL_FALSE, glm::value_ptr(mn));

    //glUniform1f(shininessUniform, shininess);

    glUniform1f(sizeUniform, tsize/5.0);

    //glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, TBO);
    glEnableVertexAttribArray(surfelAttrib);
    glEnableVertexAttribArray(normalAttrib);

    //glBeginQuery(GL_SAMPLES_PASSED, query);
    //glDrawTransformFeedback(GL_POINTS, TBO);
    glDrawArrays(GL_POINTS, 0, primitives);
    //glEndQuery(GL_SAMPLES_PASSED);

    //GLuint samples;
    //glGetQueryObjectuiv(query, GL_QUERY_RESULT, &samples);

    SDL_GL_SwapBuffers();
    //f2 << "sampls passed: " << samples << endl;
    //f2.close();
}

void onWindowResized(int w, int h)
{
    glViewport(0, 0, w, h);
    width = w; height = h;
}

void onKeyDown(SDLKey key, Uint16 /*mod*/)
{
    switch(key) {
        case SDLK_ESCAPE : quit(); return;
        case SDLK_o : mode = ORTHOGONAL; break;
        case SDLK_p : mode = PERSPECTIVE; break;

        case SDLK_d : depth = !depth; break;

        case SDLK_1 : func = GL_NEVER; break;
        case SDLK_2 : func = GL_ALWAYS; break;
        case SDLK_3 : func = GL_LESS; break;
        case SDLK_4 : func = GL_LEQUAL; break;
        case SDLK_5 : func = GL_EQUAL; break;
        case SDLK_6 : func = GL_GREATER; break;
        case SDLK_7 : func = GL_GEQUAL; break;
        case SDLK_8 : func = GL_NOTEQUAL; break;

        case SDLK_r : ray_object_changed = true; break;

            // Need to call reinit for program dependant commands
        case SDLK_t : draw_type = P_TRIANGELIZE;
            onInit();
            //glEnable(GL_CULL_FACE);
            //glCullFace(GL_FRONT);
            break;
        case SDLK_s : draw_type = P_DEF;
            onInit();
            //glEnable(GL_CULL_FACE);
            //glCullFace(GL_NONE);
            break;

        case SDLK_k : tsize++; break;
        case SDLK_l : tsize--; tsize < 1 ? tsize++ : tsize; break;

        default : return;
    }
    redraw();
}

void onKeyUp(SDLKey /*key*/, Uint16 /*mod*/)
{
}

void onMouseMove(unsigned /*x*/, unsigned /*y*/, int xrel, int yrel, Uint8 buttons)
{
    if(buttons & SDL_BUTTON_LMASK)
    {
        rx += xrel;
        ry += yrel;
        redraw();
    }
    if(buttons & SDL_BUTTON_RMASK)
    {
        pz += yrel;
        redraw();
    }
}

void onMouseDown(Uint8 button, unsigned /*x*/, unsigned /*y*/)
{
    ray_object_changed = true;
    switch(button) {
        case SDL_BUTTON_WHEELUP : wheel += 1; break;
        case SDL_BUTTON_WHEELDOWN : wheel -= 1;/* if(wheel < 1) wheel = 1; */break;
        default : return;
    };
    redraw();
}

void onMouseUp(Uint8 /*button*/, unsigned /*x*/, unsigned /*y*/)
{
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Main
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int main(int /*argc*/, char ** /*argv*/)
{
    try {
        // Init SDL - only video subsystem will be used
        if(SDL_Init(SDL_INIT_VIDEO) < 0) throw SDL_Exception();

        // Shutdown SDL when program ends
        atexit(SDL_Quit);

        init(800, 600, 24, 16, 0);

        mainLoop();

    } catch(exception & ex) {
        cout << "ERROR : " << ex.what() << endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
