#version 330

layout (triangles) in;
layout (line_strip, max_vertices=2) out;

in vec3 c[];
out vec3 passC;
out vec4 normal;
out vec2 tcoord;

uniform mat4 mvp;

void main() {

    vec4 tu,tv;
    vec3 u,v;
    vec4 tmp;

    int i;
    for (i = 0; i < gl_in.length(); i++) {

        tmp += (gl_Position = gl_in[i].gl_Position);
        u += c[i];
        passC = c[i];
        gl_Position = mvp * vec4(gl_Position.xyz,1);
        //EmitVertex();
    }

    tmp /= i;
    passC = vec3(0,1,0);
    tcoord.xy = vec2(7,7);
    gl_Position = mvp * vec4(tmp.xyz, 1);
    EmitVertex();
    //middle
//    Nx = UyVz - UzVy
//
//    Ny = UzVx - UxVz
//
//    Nz = UxVy - UyVx

    tu =  (gl_in[1].gl_Position - gl_in[0].gl_Position);
    tv =  (gl_in[2].gl_Position - gl_in[0].gl_Position);

    gl_Position = vec4(normalize(cross(tv.xyz, tu.xyz)), 1);
    tcoord.xy = vec2(7,7);
    normal = mvp * vec4(tmp.xyz + gl_Position.xyz, 1);

    // gl_Position = mvp * vec4(tmp.xyz + normalize(vec3(u.y*v.z - u.z*v.y, u.z*v.x - u.x*v.z, u.x*v.y - u.y*v.x)), 1);
    gl_Position = normal;
    passC = vec3(0,1,0);
    EmitVertex();

    EndPrimitive();
}