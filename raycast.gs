#version 330

layout (points) in; //random seeds
layout (points, max_vertices=128) out;

//in vec3 ray_origin[]; //ray normals
//in vec3 ray_dir[]; //ray normals
in uint pseed[];

out vec3 normal;
out vec3 surfel;

uniform float surface = 3;
uniform int stype = 1;

uniform float ray_box = 100; //bondary box for raycasting origins
uniform float multiply = 80; //bondary box for raycasting origins

uint rseednr;
//uint rseedr;
//uint rseedvax;

float randnr() {
    uint last = rseednr;
    rseednr = (rseednr * 1664525U + 1013904233U) ;
    return (float(last) / 4294967296.0) ;
}
/*
float randr() {
    rseedr = (rseedr * 69069U + 1U) ;
    return ray_box*(float(rseedr) / 4294967296.0)-(ray_box/2) ;
}
float randvax() {
    rseedvax = (rseedvax * 1103515245U + 12345U) ;
    return ray_box*(float(rseedvax) / 2157483648.0)-(ray_box/2) ;
}
*/
vec3 rand_vec3() {
    return vec3(randnr(), randnr(), randnr());
}

float sum(vec3 vec) {
    return vec.x + vec.y + vec.z;
}
float sum(vec4 vec) {
    return vec.x + vec.y + vec.z + vec.w;
}

float step = 0.005;

// Quadric
const vec3 MID = vec3(0.0,1.0,0.0);    // x0, y0, z0
const vec3 Q  = vec3(1.0,-1.0,1.0);     // A,  B,  C
const vec3 M  = vec3(0.0,0.0,0.0);   // F,  G,  H
const vec3 C  = vec3(0.0,0.0,-2.0);   // P,  Q,  R
float d  = surface;                    // d


mat4 P = {
        vec4(Q.x,M.z,M.y,C.x),
        vec4(M.z,Q.y,M.x,C.y),
        vec4(M.y,M.x,Q.z,C.z),
        vec4(C.x,C.y,C.z, d ),
};

//do xt P x - x dot P x
vec3 solveQan(vec3 pos, vec3 ray, mat4 Q) {
    float a = dot(vec4(ray,0), Q*vec4(ray,0));
    float b = dot(2*vec4(ray,0), Q*vec4(pos,1));
    float c = dot(vec4(pos,1), Q*vec4(pos,1));
    return normalize(vec3(a,b,c));
}

float solveQ(vec3 pos) {
    float acc;
    acc = sum((Q*(pos-MID))*(Q*(pos-MID)));
    acc = acc + sum(2*C * pos);
    pos = pos.yzx*pos.zxy;
    acc = acc + sum(2*M * pos) + d;
    return acc;
}


vec3 gradQ(vec3 pos) {
    return Q*pos + //dX^2 = 2x
    vec3( sum(vec3(M.y*pos.z, M.z*pos.y, C.x)), //dfghXYZ + dpqr
          sum(vec3(M.x*pos.z, M.z*pos.x, C.y)),
          sum(vec3(M.x*pos.y, M.y*pos.x, C.z)));
}

void main() {

    // It is possible to solve quadric implicit surfaces analtically yay... moar power
    // http://marctenbosch.com/photon/mbosch_intersection.pdf

    rseednr = pseed[0]; //rseedr = rseedvax = pseed[0]; //+uint(ray_box);

    int m = 0;
    for (m = 0; m < multiply; m++) {
        // Generate ray in box ;
        vec3 rp = ray_box * rand_vec3() - (ray_box / 2);
        vec3 rn = normalize(rand_vec3() - 0.5); // Ray normal


        //vec3 u = MID - rp;
        /*if (dot(rn, u) >= 0) {


            vec3 puv = rn * (dot(rn, u));
            float dist = distance(MID, rp + puv);
            dist *= dist;

            if (dist <= d * d) {
*/
                vec3 sol = solveQan(rp - MID, rn, P);

                float D = (sol.y * sol.y) - (4 * sol.x * sol.z);
                if (D > 0) {
                    float sD = sqrt(D);
                    float q, t1, t2;
                    if (sol.y > 0) {
                        q = -0.5 * (sol.y - sD);
                    } else {
                        q = -0.5 * (sol.y + sD);
                    }
                    t1 = q / sol.x;
                    t2 = sol.z / q;

                    if (t1 > 0) {
                        surfel = rp + rn * t1;
                        normal = normalize(gradQ(surfel-MID));
                        EmitVertex();
                        //EndPrimitive();
                    }
                    if (t2 > 0) {
                        surfel = rp + rn * t2;
                        normal = normalize(gradQ(surfel-MID));
                        EmitVertex();
                    }
                    EndPrimitive();
                }
            /*}
        }
    */}
}
