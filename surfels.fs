#version 330

in vec3 norm;
in vec3 tcoord;
in vec3 goraudColor;
in float specColor;

out vec4 fragColor;

void main() {

    // Blin-Phong model

    float d = distance(tcoord, vec3(0,0,0)); //mi-del
    //Just save specular effect
    fragColor.xyz = goraudColor * (float(int(d*10)%8)/7) + vec3(specColor);
    //fragColor.xyz = vec3(1,1,1) * (float(int(d*10)%8)/7);
    fragColor.w = 1;

    // Texture ?
}
