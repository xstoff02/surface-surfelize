#version 330

in uint seed;

out uint pseed;

void main() {
    // just send rando seed to geo shader
    pseed = seed;
}
