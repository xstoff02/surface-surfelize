#version 330

layout (points) in;
layout (triangle_strip, max_vertices=6) out;

in vec3 surf[];
in vec3 pnorm[];

out vec3 tcoord;
out vec3 norm;
out vec3 goraudColor;
out float specColor;


uniform vec3 lightPosition;
uniform mat4 mvp;
uniform mat4 mv;
uniform mat3 mn;
uniform float shininess=1;

uniform float size;
const float stype = 0;

mat4 iso = mat4(
        vec4(-1,-1,-1, 0),
        vec4( 1,-1, 1, 0),
        vec4( 1, 1,-1, 0),
        vec4(-1, 1, 1, 0))*(size / 2);

void main() {

    vec3 eyeSurf     = (mv*vec4(surf[0],1)).xyz;
    vec3 N = normalize(mn*pnorm[0]);
    vec3 L = normalize(lightPosition-eyeSurf);

    vec3 lightColor = vec3(0.2, 0.5, 0.2);
    float diffuse = max(dot(L, N), 0.0);
    lightColor += vec3(diffuse, 0.0, 0.0);

    vec3 V = normalize(-eyeSurf);
    vec3 R = reflect(-L, N);
    float specular = pow(max(dot(R, V), 0.0), shininess);

    if (stype == 0) {
        // Tesselate point to triangles
        vec4 p = vec4(size / 2, -size / 3, 0, 0);
        vec4 np = (p * vec4(1, -1, 1, 1));
        vec4 t = vec4(0, 2 * size / 3, 0, 0);

        gl_Position = mvp * vec4(surf[0], 1) + p;
        tcoord = surf[0];
        norm = N;
        specColor = specular;
        goraudColor = lightColor;
        EmitVertex();

        gl_Position = mvp * vec4(surf[0], 1) - np;
        tcoord = surf[0];
        norm = N;
        specColor = specular;
        goraudColor = lightColor;
        EmitVertex();

        gl_Position = mvp * vec4(surf[0], 1) + t;
        tcoord = surf[0];
        norm = N;
        specColor = specular;
        goraudColor = lightColor;
        EmitVertex();
    } else {

        // Tesselate point to tetrahedrons for better application of texture
        int i;
        for (i = 0; i < 7; i++) {
            norm = N;
            tcoord = (iso[i % 4].xyz + surf[0]);
            gl_Position = mvp * vec4(tcoord, 1);
            specColor = specular;
            goraudColor = lightColor;
            EmitVertex();
        }
    }
    EndPrimitive();

}