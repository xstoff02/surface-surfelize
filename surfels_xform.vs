#version 330

in vec3 surfel;
in vec3 normal;

out vec3 surf;
out vec3 pnorm;

uniform mat4 mvp;
uniform mat4 mv;
uniform mat3 mn;

uniform float size;

void main() {

    //gl_PointSize = size;
    //gl_Position = vec4(surfel,1);
    surf = surfel;
    pnorm = normal;
};

